<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Book Store</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}">--}}
{{--    <script src="{{asset('js/app.js')}}"></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
​
    <style>
        body {
            background: #ffffff;
            color: #ffffff;
            margin-top: 40px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }
​
    </style>
</head>
<body>
<br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6" align="left" style="margin: 30px">
            <h2 style="color: #000000;">แบบฟอร์มแก้ไขรองเท้า</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form action="{{route("shoes.edit")}}" method="post">
                @csrf
                <div class="form-group">
                    <label style="color: #000000;" for="products"><b>Shoes ID</b></label>
                    <input
                        type="text" class="form-control"
                        name="product_id" id="product_id"
                        aria-describedby="shoesHelp"
                        value="{{$shoes->product_id}}"
                        required
                    >
            <input type="hidden" name='id' value = "{{$shoes->id}}">
                </div>
                <div class="form-group">
                    <label style="color: #000000;" for="products"><b>ชื่อรองเท้า</b></label>
                    <input
                        type="text" class="form-control"
                        name="product_name" id="product_name"
                        aria-describedby="shoesHelp"
                        value="{{$shoes->product_name}}"
                        required
                    >

                </div>
                
                <div class="form-group">
                    <label style="color: #000000;" for="products"><b>Price</b></label>
                    <input type="text" class="form-control" name="price" id="price" value="{{$shoes->price}}" placeholder="ราคารองเท้า">
                </div>
                <div class="form-group">
                    <label style="color: #000000;" for="products"><b>Count</b></label>
                    <textarea type="text" class="form-control" id="count" name="count">{{$shoes->count}} </textarea>
                </div>

                <br>
                <div align="right">
                    <button type="submit" style="width: 150px" class="btn btn-success">SUBMIT EDIT</button>
                    <button type="reset"  style="width: 100px" class="btn btn-danger"><b><a style="color:white;" href="{{route("home")}}">CANCEL</a></b></button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>