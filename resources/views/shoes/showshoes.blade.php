<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shoes Store</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css">--}}
{{--    <script src="{{asset('js/app.js')}}" defer></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style>
        body {
            background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR7cLDL6eoqKWYuh3O6HX0Hs-1_jpszDa7tbQ0oYDxyoeAkROjo");
            background-repeat: no-repeat;
            background-size: cover;
            color: #000000;
            margin-top: 40px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }
​
    </style>
</head>
<h1 align="center">Shoes Shop</h1>
<body>
<br><br>
<div class="container">
    <div class="row" style ="background-color:#fff;padding:20px; border-radius:10px;">
        <div class="col-12">
        <form action="{{route("buy")}}" method="post">
            <table class="table table-hover table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ลับดับที่</th>
                    <th scope="col">ชื่อรองเท้า</th>
                    <th scope="col">ราคา</th>
                    <th scope="col">จำนวนที่เหลือ</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($shoes as $key => $Shoes)
                    <tr>
                        <th scope="row">{{$key + 1}}</th>
                        <td>{{$Shoes->product_name}}</td>
                        <td>{{$Shoes->price}}</td>
                        <td>{{$Shoes->count}}</td>
                        <td><a href="{{route('buy', $Shoes->id)}}" class="btn btn-warning">ซื้อ</a></td>
                        </tr>
                @endforeach
                </tbody>
            </table>
        </form>
        </div>
    </div>
</div>
</body>
</html>