<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Shoes</title>
    {{--    <link rel="stylesheet" href="{{asset('css/app.css')}}">--}}
    {{--    <script src="{{asset('js/app.js')}}"></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
​
    <style>
        body {
            background: #ffffff;
            color: #ffffff;
            margin-top: 40px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }
​
    </style>
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6" align="left" style="margin: 30px">
            <h2 style="color: #000000;">แบบฟอร์มเพิ่มรองเท้า</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form action="{{route("addsuccess")}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="product_id" style="color: #000000;" ><b>Product_id</b></label>
                    <input
                        type="text" class="form-control"
                        name="productid"
                        required >
                </div>
                <div class="form-group">
                    <label for="product_name" style="color: #000000;" ><b>Product_name</b></label>
                    <input
                        type="text" class="form-control"
                        name="productname"
                        required >
                </div>
                <div class="form-group">
                    <label for="price" style="color: #000000;" ><b>Price</b></label>
                    <input
                        type="text" class="form-control"
                        name="price"
                        required >
                </div>
                <div class="form-group">
                    <label for="count" style="color: #000000;" ><b>Count</b></label>
                    <input
                        type="text" class="form-control"
                        name="count"
                        required >
                </div>
                <br>
                <div align="right">
                    <button type="submit" style="width: 100px" class="btn btn-success"><b>ADD</b></button>
                    <button type="reset"  style="width: 100px" class="btn btn-danger"><b><a style="color:white;" href="{{route("home")}}">CANCEL</a></b></button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>

