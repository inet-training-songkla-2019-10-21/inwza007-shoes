<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', 'ProductsController@showProducts')->name('products.products');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
require 'admin_add_products.php';
require 'route_user_show_products.php';
require 'userlogin.php';
require 'route_user_show_products.php';
require 'route_edit_products.php';