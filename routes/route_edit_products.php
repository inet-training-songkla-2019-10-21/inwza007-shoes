<?php

Route::get('/admin/edit', 'EditController@index')->name('shoes.index');
// Route::group(['middleware' => ['user']], function(){
    Route::get('/admin/edit/{id}', 'EditController@editPage')->name('shoes.edit.page');
    Route::post('/admin/edit', 'EditController@edit')->name('shoes.edit');
// });

