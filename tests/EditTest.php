<?php

namespace Tests\Feature;

use App\Shoes;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShoesTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAdminCanEditShoes()
    {
        $shoes = factory(Shoes::class)->create();
        $newShoes = factory(Shoes::class)->make();

        $this->get(route('shoes.edit.page', $shoes->id))
            ->assertViewIs('shoes.edit')
            ->assertSee($shoes->product_id)
            ->assertSee($shoes->product_name)
            ->assertSee($shoes->price)
            ->assertSee($shoes->count)
            ->assertStatus(200);

        $this->post(route('shoes.edit'), [
            '_token' => \Session::token(),
            'product_id' => $shoes->product_id,
            'product_name' => $newShoes->product_name,
            'price' => $newShoes->price,
            'count' => $newShoes->count,
        ])->assertRedirect(route('shoes.index'));

        $shoesEdited = Shoes::find($shoes->id);
        $this->assertEquals($newShoes->product_id, $shoesEdited->product_id);
        $this->assertEquals($newShoes->product_name, $shoesEdited->product_name);
        $this->assertEquals($newShoes->price, $shoesEdited->price);
        $this->assertEquals($newShoes->count, $shoesEdited->count);
    }
}

