<?php

namespace Tests\Feature;

use App\Products;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGuestCanSeeProductsPage()
    {
        $response = $this->get(route('products.index'));

        $response->assertViewIs('products.index')
            ->assertStatus(200);
    }

    public function testGuestCanSeeAllProductsExist()
    {
        $products = factory(Products::class, 5)->create();

        $response = $this->get(route('products.index'))
            ->assertSuccessful();

        $response->assertSee($products[0]->name);
        $response->assertSee($products[1]->name);
        $response->assertSee($products[2]->name);
        $response->assertSee($products[3]->name);
        $response->assertSee($products[4]->name);

    }
}
