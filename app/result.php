<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class result extends Model
{
    protected $fillable =[
        'email',
        'product_id',
        'price',
        'count',
    ];
}

