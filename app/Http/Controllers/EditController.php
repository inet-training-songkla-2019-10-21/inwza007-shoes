<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;

class EditController extends Controller
{
    // public function index()
    // {
    //     $shoes = Shoes::all();
    //     // dd($shoes);
    //     return view('shoes.index', compact('shoes'));
    // }
   
 public function editPage($id)
    {
        $shoes = Products::find($id);
        // dd($shoes);
        return view('edit', ['shoes' => $shoes]);
    }

    public function edit(Request $request)
    {
        // dd($request->all());
        $shoes = Products::find($request->id);
        // dd($request->id);
        $shoes->product_id = $request->product_id;
        $shoes->product_name = $request->product_name;
        $shoes->price = $request->price;
        $shoes->count = $request->count;

        if (!$shoes->save()) {
            return redirect()->route('shoes.edit.page');
        }
        return redirect()->route('products.products');
    }
}

