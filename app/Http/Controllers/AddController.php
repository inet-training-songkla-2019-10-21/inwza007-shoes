<?php

namespace App\Http\Controllers;
use App\Products;
use Illuminate\Http\Request;

class AddController extends Controller
{
    public function addPage()
    {
        return view('add');
        
    }

    public function addProducts(Request $request)
    {
        
        $product = new Products();
        $product->product_id = $request->productid;
        $product->product_name = $request->productname;
        $product->price = $request->price;
        $product->count = $request->count;

        if (!$product->save()){
            return redirect()->route('add');
        }
        return redirect()->route('products.products');

    }
}

