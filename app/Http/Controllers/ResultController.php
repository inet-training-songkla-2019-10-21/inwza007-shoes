<?php

namespace App\Http\Controllers;
use App\Buys;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    public function showResult()
    {
        $buys = Buys::all();
        return view('result',compact('buys'));
    }
}

