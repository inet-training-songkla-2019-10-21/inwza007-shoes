<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Products;

class ProductsController extends Controller
{
    public function showProducts()
    {
        $products = Products::all();
        return view('products.products', compact('products'));
        
    }
}
