<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buys extends Model
{
    protected $fillable = [
        'email',
        'product_id',
        'price',
        'count',
    ];
    public function user()
    {
        return $this->belongsToMany(User::class, 'created_by', 'id');
    }
}
