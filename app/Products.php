<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'product_id',
        'product_name',
        'price',
        'count',
    ];
    public function user()
    {
        return $this->belongsToMany(User::class, 'created_by', 'id');
    }
}
